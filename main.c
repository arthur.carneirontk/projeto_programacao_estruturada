#include <stdio.h>
#include <stdlib.h>

int verificaArquivo(char *nomeDoArquivo);

void criaArquivo(char *nomeDoArquivo);


void criaArquivo();

int main() {

    //(CREATE)Função que verifica se o arquivo existe, caso não exista este arquivo é criado.
    if (verificaArquivo("C:\\Users\\Arthur\\Desktop\\Code\\untitled\\jogadores.txt") == 0) {
        printf("O usuario optou por nao criar o arquivo!");
    } else {
        criaArquivo("C:\\Users\\Arthur\\Desktop\\Code\\untitled\\jogadores.txt");
    }

    //(READ)Função

    return 0;
}

void criaArquivo(char *nomeDoArquivo) {

    FILE *file;
    printf("Criando arquivo....");
    file = fopen(nomeDoArquivo, "w");
    if (file != NULL) {
        printf("O arquivo %s foi criado com sucesso!", nomeDoArquivo);
    } else {
        printf("Ocorreu um erro desconhecido!");
    }

}

int verificaArquivo(char *nomeDoArquivo) {
    FILE *file;
    int escolha;
    file = fopen(nomeDoArquivo, "r");

    if (file == NULL) {
        printf("O arquivo nao existe\n");
        printf("Deseja criar o arquivo? 1 - SIM || 2 - NAO\n");
        scanf("%i", &escolha);
        fflush(stdin);
        while (escolha > 2 || escolha <= 0) {
            printf("Escolha invalida, insira outro valor (ENTRE 1 E 2)\n");
            scanf("%i", &escolha);
            system("clear");
        }
        if (escolha == 1) {
            return 1;
        } else if (escolha == 2) {
            return 0;
        }
        fclose(file);
    } else {
        printf("O arquivo %s ja existe no disco rigido!", nomeDoArquivo);
        fclose(file);
    }
}